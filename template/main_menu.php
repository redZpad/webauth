<?php
$len = 12;
$menu = [
	[
		'title' => 'Главная',
		'sort' => 0,
		'path' => '/',
	],
	[
		'title' => 'Каталог',
		'sort' => 4,
		'path' => '/route/market/',
	],
	[
		'title' => 'О нас',
		'sort' => 2,
		'path' => '/route/about/',
	],
	[
		'title' => 'Новости',
		'sort' => 1,
		'path' => '/route/news/',
	],
	[
		'title' => 'Контакты',
		'sort' => 3,
		'path' => '/route/contacts/',
	],
];


function showMenu($menu, $class = "", $sort = SORT_ASC) {
    $url = $_SERVER["REQUEST_URI"];
    $menu = arraySort($menu, $sort);
    require $_SERVER['DOCUMENT_ROOT'] . '/template/menu.php'; 	
};


function arraySort ($menu, $sort = SORT_ASC, $key = 'sort')
{
    usort($menu, function($a, $b) use ($key, $sort)
    {
        return $sort === SORT_DESC ? $b[$key] <=> $a[$key] : $a[$key] <=> $b[$key];
	});
	return $menu;
};

function getTitle ($menu) {
    $url = $_SERVER["REQUEST_URI"];
    $menu = arraySort($menu);
    foreach ($menu as $id => $menuItem) {
        if ($menuItem['path'] == $url) {
            return $menuItem['title'];
        }
    }
};

function cut($string, $len) {
    if (strlen($string) > $len) {
    $string = substr ($string, 0, $len) . '...';
    }
    return $string;
};