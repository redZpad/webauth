<ul class="main-menu <?=$class?>">
    <?php
    foreach ($menu as $menu) {
        $style = ($url == $menu['path']) ? 'style="text-decoration: underline;"' : 'style="text-decoration: none;"' ;
    ?>
        <li><a href="<?=$menu['path']?>"<?=$style?>>
            <?=$menu['title']?>	
            </a>
        </li>
    <?php } ?>
</ul>