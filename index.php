<?php
include 'template/main_menu.php';
$success = false;
$error = false;
if ((! empty($_POST['login'])) && (! empty($_GET))) {
    require(__DIR__ . '/include/logins.php');
    require(__DIR__ . '/include/passwords.php');
    $index = array_search($_POST['login'], $logins);
    if ($index !== false && $_POST['password'] == $passwords[$index]) {
        $success = true;
    } else {
        $error = true;
    }
}
include 'template/header.php';
?>	
				<h1>Возможности проекта —</h1>
				<p>Вести свои личные списки, например покупки в магазине, цели, задачи и многое другое. Делится списками с друзьями и просматривать списки друзей.</p>
             <?php if (isset($_GET['login']) && $_GET['login'] == 'yes') { ?>
			</td>
            <td class="right-collum-index">
				<div class="project-folders-menu">
					<ul class="project-folders-v">
    					<li class="project-folders-v-active"><a href="#">Авторизация</a></li>
    					<li><a href="#">Регистрация</a></li>
    					<li><a href="#">Забыли пароль?</a></li>
					</ul>
				    <div class="clearfix"></div>
				</div>
                 
				<div class="index-auth">
				<?php
				if ($error) {include 'include/error.php';}
				if ($success) {include 'include/success.php';}
				?>	
                    <form action="/?login=yes" method="POST">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td class="iat">
                                    <label for="login_id">Ваш e-mail:</label>
                                    <input id="login_id" size="30" name="login"
                                    <?php $errLog = (! empty($_POST['login'])) ? $_POST['login'] : ""?> value = "<?= $errLog?>">
                                </td>
							</tr>
							<tr>
								<td class="iat">
                                    <label for="password_id">Ваш пароль:</label>
                                    <input id="password_id" size="30" name="password" type="password"
                                    <?php $errPas = (! empty($_POST['password'])) ? $_POST['password'] : ""?> value = "<?= $errPas?>">
                                </td>
							</tr>
							<tr>
								<td><input type="submit" value="Войти">
								</td>	
							</tr>
						</table>
                    </form>
				</div>
			<?php } 
            ?>
			</td>
        </tr>
    </table>
<?php    
include 'template/footer.php';
?>